# AND Initiatives API

This API exposes one endpoint which are used to get a list of Initiatives:

**HTTP GET** `https://localhost:8080/api/initiative/{id}`


## Built With

# Development Environment 

## Build Image
```
$ cd backend
$ mvn clean install
$ docker build -t and-initiatives .
```

## Run Container
```
$ docker run -it -p 80:8080 --name and-initiatives --rm and-initiatives
```

# Deployment
The application is deployed to AND's central AWS account. It uses an EC2 instance to run the docker container.

More stuff to come!