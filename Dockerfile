FROM openjdk:8

ARG ENV_NAME='development'

ADD target/initiatives-mysql.jar initiatives-mysql.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "initiatives-mysql.jar"]

# CMD exec java -jar initiatives-mysql.jar $ENV_NAME