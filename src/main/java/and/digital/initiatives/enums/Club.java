package and.digital.initiatives.enums;

public enum Club {

        ADA("ada"),
        DEKKER("dekker"),
        HAMILTON("hamilton"),
        KILBURN("kilburn"),
        MURRAY("murray"),
        NEWTON("newton"),
        SOMERVILLE("somerville"),
        SPARCK("sparck"),
        TURING("turing"),
        BHAG_TEAMS("bhag-teams"),
        COMPANY("company"),
        CONSULTING("consulting"),
        TENZING_NORTH("tenzing-north"),
        TENZING_SOUTH("tenzing-south"),
        THE_LINK("the-link");

    private String clubName;
    Club(String clubName) {
        this.clubName = clubName;
    }

    @Override
    public String toString() {
        return this.clubName;
    }

    public static boolean validateClubName(String clubName) {
        for (Club club:
             Club.values()) {
            if (clubName.equals(club.toString())) {
                return true;
            }
        }
        return false;
    }
}
