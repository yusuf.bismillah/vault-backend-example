package and.digital.initiatives.controller;

import and.digital.initiatives.enums.Club;
import and.digital.initiatives.exception.ErrorMessage;
import and.digital.initiatives.exception.ErrorMessageType;
import and.digital.initiatives.exception.InitiativeException;
import and.digital.initiatives.model.Initiative;
import and.digital.initiatives.model.User;
import and.digital.initiatives.service.InitiativesService;
import and.digital.initiatives.service.UserService;
import and.digital.initiatives.sorter.InitiativesSortByDate;
import and.digital.initiatives.sorter.InitiativesSortByRelevance;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/api/initiatives")
@NoArgsConstructor
@Log4j2
public class InitiativesController {

    @Autowired
	private InitiativesService service;

    @Autowired
    private UserService userService;

	@Value("${application.front.end:https://dev-the-vault.and-newton.co.uk}")
	private String frontEndUrl;

	private static final String INVALID_CLUB_NAME = " is invalid club name";
	private static final String LEAD_IS_REQUIRED ="Lead is required";

    public InitiativesController(InitiativesService service) {
        this.service = service;
    }
    
    @GetMapping
    public ResponseEntity<?> listAll(@RequestParam(value = "page") int page) {
        log.info("Listing all initiatives page wise");
    	Pageable pageable = PageRequest.of(page - 1, 5);
        Page<Initiative> pageContent =  service.findAll(pageable);
        List<Initiative> initiatives = initiativesMappingForList(pageContent.getContent());
        return ResponseEntity.status(HttpStatus.OK).body(initiatives);
    }
    
    @GetMapping(path="/all",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<?> listAllInitiatives() {
        log.info("Listing all initiatives");
    	List<Initiative> results =  service.findAll();
        results = initiativesMappingForList(results);
        results.sort(new InitiativesSortByDate());
        Collections.reverse(results);
        return ResponseEntity.status(HttpStatus.OK).body(results);
    }
    
    @PostMapping(
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )

    public ResponseEntity<?> createInitiative( @Valid  @RequestBody Initiative initiative) {
        log.info("Saving initiative");
        try {
            // Validate club name
            if (!validateClubNames(initiative.getClub())) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body(new ErrorMessage(ErrorMessageType.BAD_REQUEST.getValue(),initiative.getClub() + INVALID_CLUB_NAME));
            }
            if(initiative.getLeads().size() == 0) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body(new ErrorMessage(ErrorMessageType.BAD_REQUEST.getValue(),LEAD_IS_REQUIRED));
            }

            //Check for leads
            Set<User> users = setOnlyOneLead(initiative);
            initiative.setLeads(users);
            //initlead field is required column in Initiative table. but no longer used in frontend after adding leads.
            // To avoid exception set it to empty string, remove it after removing initlead field from Initiative table in future. Use
            // database script migration tool like liquibase for this purpose.
            initiative.setInitlead("");

            Initiative createdInitiative = service.save(initiative);
            //Remove leads from user response
            createdInitiative = usersMappingForInitiativeResponse(createdInitiative);
            return ResponseEntity.status(HttpStatus.CREATED).body(createdInitiative);
        } catch (InitiativeException e) {
            ErrorMessage errorMessage = new ErrorMessage(ErrorMessageType.BAD_REQUEST.getValue(), e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorMessage);
        }

    }
    
    @PutMapping(path="/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<?> updateInitiative(@RequestBody Initiative initiative, @PathVariable("id") long id) {
        log.info("Updating initiative for id:"+id);
        // Validate club name
        if (!validateClubNames(initiative.getClub())) {
            ErrorMessage errorMessage = new ErrorMessage(ErrorMessageType.BAD_REQUEST.getValue(), initiative.getClub() + INVALID_CLUB_NAME);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new ErrorMessage(ErrorMessageType.BAD_REQUEST.getValue(), initiative.getClub() + INVALID_CLUB_NAME));
        }
        if(initiative.getLeads().size() == 0) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new ErrorMessage(ErrorMessageType.BAD_REQUEST.getValue(), LEAD_IS_REQUIRED));
        }
    	Optional<Initiative>  optional = service.findById(id);
    	if (optional.isPresent()) {
            Initiative initiativeOld = optional.get();
            initiativeOld.setTitle(initiative.getTitle());
            initiativeOld.setCategory(initiative.getCategory());
            initiativeOld.setClub(initiative.getClub());
            //InitLead field no not uses, set it empty string as it is required column in database.
            // Remove this line of code when InitLead field is removed from Initiative table.
            initiativeOld.setInitlead("");
            initiativeOld.setActive(initiative.getActive());
            initiativeOld.setDescription(initiative.getDescription());
            initiativeOld.setCreateddate(initiative.getCreateddate());
            Set<User> users = setOnlyOneLead(initiative);
            initiativeOld.setLeads(users);

            Initiative updatedInitiative = service.update(initiativeOld);
            updatedInitiative = usersMappingForInitiativeResponse(updatedInitiative);
            return ResponseEntity.status(HttpStatus.OK).body(updatedInitiative);
        } else {
    	    return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

    }

    @DeleteMapping(value="/{id}")
    public ResponseEntity<?> deleteInitiative(@PathVariable("id") long id) {
        log.info("Deleting initiative for id:"+id);
        service.delete(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @GetMapping(value="/{id}")
    public ResponseEntity<?> getOne(@PathVariable("id") long id) {
        log.info("Retrieving initiative for id:"+id);
        Optional<Initiative> initiative = service.findById(id);
        if (initiative.isPresent()) {
            Initiative initiativeResponse = initiative.get();
            initiativeResponse = usersMappingForInitiativeResponse(initiativeResponse);
            return ResponseEntity.status(HttpStatus.OK).body(initiative.get());
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }
    
    @GetMapping(path="/search",
    produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<?> searchInitiatives(@RequestParam String searchTerm) {
        log.info("Search initiative for: "+searchTerm);
        if (StringUtils.hasText(searchTerm)) {
            searchTerm = searchTerm.toLowerCase();
            List<Initiative> results = service.findInitiativeBySearchTerm(searchTerm);
            results = initiativesMappingForList(results);
            results.sort(new InitiativesSortByRelevance());
            return ResponseEntity.status(HttpStatus.OK).body(results);
        } else {
            //for searchterm contining without text return empty arraylist
            return ResponseEntity.status(HttpStatus.OK).body(new ArrayList<>());
        }
    }
    
    @GetMapping(path="/club/{club}",
    produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<?> filterOnClub(@PathVariable("club") String clubSelected, @RequestParam(value = "page") int page) {
        // Validate club name
        if (!validateClubNames(clubSelected)) {
            ErrorMessage errorMessage = new ErrorMessage(ErrorMessageType.BAD_REQUEST.getValue(), clubSelected + INVALID_CLUB_NAME);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorMessage);
        }
        log.info("Filter on club: "+ clubSelected);
        clubSelected = clubSelected.toLowerCase();
        List<Initiative> results = service.findInitiativesByClub(clubSelected);
        results = initiativesMappingForList(results);
        return ResponseEntity.status(HttpStatus.OK).body(newPage(results, page));
    }
    
    @GetMapping(path="/category/{category}",
    produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<?> filterOnCategory(@PathVariable("category") String categorySelected, @RequestParam(value = "page") int page) {
        log.info("Filter on category: "+ categorySelected);
    	categorySelected = categorySelected.toLowerCase();
        List<Initiative> results = service.findInitiativesByCategory(categorySelected);
        results = initiativesMappingForList(results);
        return ResponseEntity.status(HttpStatus.OK).body(newPage(results, page));
    }

    @GetMapping(path="/club/{club}/search",
    produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<?> searchInitiativesFilteredOnClub(@PathVariable("club") String clubGiven, @RequestParam String searchTerm) {
        // Validate club name
        if (!validateClubNames(clubGiven)) {
            ErrorMessage errorMessage = new ErrorMessage(ErrorMessageType.BAD_REQUEST.getValue(), clubGiven + INVALID_CLUB_NAME);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorMessage);
        }
        log.info("Filter on club: "+clubGiven+ " , search: "+ searchTerm);
        if (StringUtils.hasText(searchTerm)) {
            clubGiven = clubGiven.toLowerCase();
            searchTerm = searchTerm.toLowerCase();
            List<Initiative> results = service.findInitiativeByClubAndSearchTerm(clubGiven, searchTerm);
            results = initiativesMappingForList(results);
            results.sort(new InitiativesSortByRelevance());
            return ResponseEntity.status(HttpStatus.OK).body(results);
        } else {
            //for searchterm contining without text return empty arraylist
            return ResponseEntity.status(HttpStatus.OK).body(new ArrayList<>());
        }

    }
    
    @GetMapping(path="/category/{category}/search",
    produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<?> searchInitiativesFilteredOnCategory(@PathVariable("category") String categoryGiven, @RequestParam String searchTerm) {
        log.info("Filter on category: "+categoryGiven+ " , search: "+ searchTerm);
        if (StringUtils.hasText(searchTerm)) {
            categoryGiven = categoryGiven.toLowerCase();
            searchTerm = searchTerm.toLowerCase();
            List<Initiative> results = service.findInitiativeByCategoryAndSearchTerm(categoryGiven, searchTerm);
            results = initiativesMappingForList(results);
            results.sort(new InitiativesSortByRelevance());
            return ResponseEntity.status(HttpStatus.OK).body(results);
        } else {
            //for searchterm contining without text return empty arraylist
            return ResponseEntity.status(HttpStatus.OK).body(new ArrayList<>());
        }

    }
    //<to do> check if front end is not using remove it
   @GetMapping(value="/profile", produces = {"application/json"})
    public String securedPage(//Model model,
                             // @RegisteredOAuth2AuthorizedClient OAuth2AuthorizedClient authorizedClient,
                              java.security.Principal  oauth2User) {
        log.info("Retrieving profile details");
        /*model.addAttribute("userName", oauth2User.getName());
        model.addAttribute("clientName", authorizedClient.getClientRegistration().getClientName());  
        model.addAttribute("userAttributes", oauth2User.getAttributes()); 
        return oauth2User.getAttributes().get("name").toString();*/
        return oauth2User.getName();
    }
    
    /*@GetMapping(value = "/redirect")
    public ModelAndView method() {
        return new ModelAndView("redirect:" + frontEndUrl + '/');
    }*/

    private List<Initiative> newPage(List<Initiative> results, int page) {
        List<Initiative> newPage = new ArrayList<>();

        for(int i = page * 5 - 5; i < page * 5; i++) {
            if(results.size() > i) {
                newPage.add(results.get(i));
            }
        }
        return newPage;
    }
    // Added to check the health status of AWS Load balancer
    @GetMapping(value = "/health", produces = {"application/json"})
    public String health() {
        log.info("Retrieving health details");
        return "service running";
    }

    private boolean validateClubNames(String club) {
       return Club.validateClubName(club);
    }

    //API /{initiativeId}/leads/{leadId} add or remove is not required till front end support multilead}
    /*@PutMapping(path ="/{initiativeId}/leads/{leadId}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<?> addUserAsLead(@PathVariable("initiativeId") Long initiativeId, @PathVariable("leadId") Long leadId) {
        Optional<Initiative> findInitiative = this.service.findById(initiativeId);
        if (findInitiative.isPresent()) {
            Optional<User> findUser = this.userService.getUserById(leadId);
            if (findUser.isPresent()) {
                Initiative initiative = findInitiative.get();
                User user = findUser.get();
                initiative.getLeads().add(user);
                initiative = this.service.update(initiative);
                return ResponseEntity.status(HttpStatus.OK).body(initiative);
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorMessage(ErrorMessageType.BAD_REQUEST.getValue(),"User with id " + leadId + " does not exist"));
            }
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorMessage(ErrorMessageType.BAD_REQUEST.getValue(),"Initiative with id " + initiativeId + " does not exist"));
        }
    }


    @DeleteMapping(path ="/{initiativeId}/leads/{leadId}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<?> removeUserAsLead(@PathVariable("initiativeId") Long initiativeId, @PathVariable("leadId") Long leadId) {
        Optional<Initiative> findInitiative = this.service.findById(initiativeId);
        if (findInitiative.isPresent()) {
            Optional<User> findUser = this.userService.getUserById(leadId);
            if (findUser.isPresent()) {
                Initiative initiative = findInitiative.get();
                User user = findUser.get();
                initiative.getLeads().remove(user);
                initiative = this.service.update(initiative);
                return ResponseEntity.status(HttpStatus.OK).body(initiative);
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorMessage(ErrorMessageType.BAD_REQUEST.getValue(),"User with id " + leadId + " does not exist"));
            }
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorMessage(ErrorMessageType.BAD_REQUEST.getValue(), "Initiative with id " + initiativeId + " does not exist"));
        }
    }*/

    //For List<Initiatives> response remove the leads and links, the single initiative response will contain these fields
    private List<Initiative> initiativesMappingForList(List<Initiative> initiatives) {
        List<Initiative> initiativeList = new ArrayList<>();
        for(Initiative initiative: initiatives) {
            initiative.setLeads(new HashSet<>());
            initiative.setLinks(new HashSet<>());
            initiativeList.add(initiative);
        }
        return initiativeList;
    }

    private Set<User> setOnlyOneLead(Initiative initiative) {
        Set<User> leads= new HashSet<>();
        if(initiative.getLeads().size() != 0) {
            for(User user: initiative.getLeads()) {
                user.getId();
                Optional<User> findUser = this.userService.getUserById(user.getId());
                if(findUser.isPresent()) {
                    leads.add(user);
                }
                //Because front end support only one lead
                break;
            }

        }
        return leads;
    }

    private Initiative usersMappingForInitiativeResponse(Initiative initiative) {
        Set<User>  users = new HashSet<>();
        for(User lead: initiative.getLeads()) {
            lead.setInitiativeAsLead(new HashSet<>());
            users.add(lead);
        }
        initiative.setLeads(users);
        return initiative;

    }

}
