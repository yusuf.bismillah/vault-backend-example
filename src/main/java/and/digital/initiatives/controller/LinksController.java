package and.digital.initiatives.controller;

import and.digital.initiatives.model.Initiative;
import and.digital.initiatives.model.Link;
import and.digital.initiatives.service.InitiativesService;
import and.digital.initiatives.service.LinksService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/initiatives")
@AllArgsConstructor
public class LinksController {

	private LinksService linkService;
	private InitiativesService initiativesService;
	
    @RequestMapping(value="/{id}/links",method = RequestMethod.POST,
	        produces = "application/json",
			consumes = "application/json"
	)
    public ResponseEntity<?> createLink(@RequestBody Link link, @PathVariable("id") long initId) {
    	Optional<Initiative> initiative = initiativesService.findById(initId);
    	if(initiative.isPresent()) {
			link.setInitiative(initiative.get());
			Link createdLink = linkService.save(link);
			return ResponseEntity.status(HttpStatus.CREATED).body(createdLink);
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}

    }
    
    
    @RequestMapping(value="/{id}/links",method = RequestMethod.GET)
    public List<Link> listAll(@PathVariable("id") long initId) {
    	List<Link> links = linkService.findAll();
    	Optional<Initiative> initiative = initiativesService.findById(initId);

    	List<Link> usefulLinks = new ArrayList<>();
    	for(Link link : links) {
    		if(link.getInitiative().equals(initiative.get())) {
    			usefulLinks.add(link);
    		}
    	}
    	return usefulLinks;
    }
    
    @RequestMapping(value="/{id}/links",method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void deleteLink(@PathVariable("id") long initId) {
    	List<Link> links = linkService.findAll();

    	Optional<Initiative> initiative = initiativesService.findById(initId);

    	for(Link link : links) {
    		if (initiative.isPresent()) {
				if(link.getInitiative() != null && link.getInitiative().getId() == initiative.get().getId()) {
					linkService.delete(linkService.findById(link.getId()));
				}
			}

    	}
    }
    
    //Used for testing
    @RequestMapping(value="/links",method = RequestMethod.GET)
    public List<Link> listAllTest() {
    	return linkService.findAll();
    }
}
