package and.digital.initiatives.controller;

import and.digital.initiatives.exception.ErrorMessage;
import and.digital.initiatives.exception.ErrorMessageType;
import and.digital.initiatives.model.Initiative;
import and.digital.initiatives.model.User;
import and.digital.initiatives.service.InitiativesService;
import and.digital.initiatives.service.UserService;
import and.digital.initiatives.sorter.UserSortByFirstName;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping( "/api/initiatives/users")
@NoArgsConstructor
@Log4j2
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private InitiativesService initiativesService;

    // POST, PUT, GET, DELETE User is restricted for AND users. Ony user search is allowed for ANDIs through api call.
    // Only Back End Admin can do these operations. Back end admin needs okta auth token also
    // to access user resource in addition to APITOKEN.
    // In future when user integration is implemented may be
    // by importing csv file, add job to update user and update authorization
    // accordingly for user resource.
    // The uuid token for admin and use following header in request header:
    // requester : token
    private static final String ADMIN_HEADER = "requester";
    private static final String ADMIN_UUID = "e4cde203-ddc2-4798-8449-4c59f71171e7";

    @PostMapping(
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<?> createUser(@Valid  @RequestBody User user, @RequestHeader(value = ADMIN_HEADER, required = true) String token) {
        if (!token.equals(ADMIN_UUID)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        try {
            User createdUser = this.userService.createUser(user);
            log.info("User with emaild " + user.getEmail() + " created");
            return ResponseEntity.status(HttpStatus.CREATED).body(createdUser);
        } catch(Exception e) {
            ErrorMessage errorMessage = new ErrorMessage(ErrorMessageType.BAD_REQUEST.getValue(), e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorMessage);
        }
    }

    @PutMapping(path = "/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<?> updatUser(@Valid @RequestBody User user, @PathVariable("id") Long id,  @RequestHeader(value = ADMIN_HEADER, required = true) String token) {
        if (!token.equals(ADMIN_UUID)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        Optional<User> findUser = this.userService.getUserById(id);
        if (findUser.isPresent()) {
            User oldUser = findUser.get();
            oldUser.setFirstName(user.getFirstName());
            oldUser.setLastName(user.getLastName());
            oldUser.setEmail(user.getEmail());
            oldUser.setSquad(user.getSquad());
            oldUser.setClub(user.getClub());
            User updatedUser = this.userService.updateUser(oldUser);
            log.info("User with emaild " + user.getEmail() + " updated");
            return ResponseEntity.status(HttpStatus.OK).body(updatedUser);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @GetMapping(path = "/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE

    )
    public ResponseEntity<?> getUserById(@PathVariable("id") Long id,  @RequestHeader(value = ADMIN_HEADER, required = true) String token) {
        if (!token.equals(ADMIN_UUID)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        Optional<User> user = this.userService.getUserById(id);
        if (user.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(user.get());
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @GetMapping(path = "/search",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<?> getUsersBySearchTerm(@RequestParam("searchTerm") String searchTerm) {
        if (StringUtils.hasText(searchTerm) && searchTerm.length() > 2) {
            searchTerm = searchTerm.replaceAll("  "," ");
            searchTerm = searchTerm.toLowerCase();
            List<User> users;
            if(searchTerm.contains(" ")) {
                String[] name = searchTerm.split(" ");
                users = this.userService
                        .findUserByFirstNameOrLastName(name[0], name[1]);
                if(users.size() == 0) {
                    users = this.userService.findUserByFirstNameOrLastName(name[1], name[0]);
                }
            } else {
                users = this.userService.findUserByFirstNameOrLastName(searchTerm, searchTerm);
            }
            users.sort(new UserSortByFirstName());
            return ResponseEntity.status(HttpStatus.OK).body(users);
        } else {
            //for searchterm contining without text return empty arraylist
            return ResponseEntity.status(HttpStatus.OK).body(new ArrayList<>());
        }


    }

    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<?> getUsers( @RequestHeader(value = ADMIN_HEADER, required = true) String token) {
        if (!token.equals(ADMIN_UUID)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        List<User> users = this.userService.findAllUser();
        return ResponseEntity.status(HttpStatus.OK).body(users);

    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<?> deleteUserById(@PathVariable("id") Long id,  @RequestHeader(value = ADMIN_HEADER, required = true) String token) {
        if (!token.equals(ADMIN_UUID)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        Optional<User> findUser = this.userService.getUserById(id);
        if(findUser.isPresent()) {
            User user = findUser.get();
            for(Initiative initiative: user.getInitiativeAsLead()) {
                initiative.getLeads().remove(user);
                this.initiativesService.update(initiative);
            }
            this.userService.deleteUser(user.getId());
            log.info("User with emaild " + user.getEmail() + " deleted");
            return ResponseEntity.status(HttpStatus.OK).build();
        } else {
            return ResponseEntity.status(HttpStatus.OK).build();
        }
    }


}
