package and.digital.initiatives.service;

import and.digital.initiatives.exception.UserException;
import and.digital.initiatives.model.User;
import and.digital.initiatives.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public User createUser(User user) {
        List<User> users = this.userRepository.findUsersByEmail(user.getEmail());
        if(users.size() > 0) {
            throw new UserException("User with Email id" + user.getEmail() + " already exist");
        }
        return this.userRepository.save(user);
    }
    public User updateUser(User user) {
        return this.userRepository.save(user);
    }

    public Optional<User> getUserById(Long id) {
        return this.userRepository.findById(id);
    }

    public void deleteUser(Long id) {
        this.userRepository.deleteById(id);
    }

    public List<User> findAllUser() {
        return this.userRepository.findAll();
    }

    public List<User> findUserBySearchterm(String searchterm) {
        return this.userRepository.findUserBySearchterm(searchterm);
    }

    public List<User> findUserByFirstNameOrLastName(String firstName, String lastName) {
        return this.userRepository.findUserByFirstNameLastName(firstName, lastName);
    }
}
