package and.digital.initiatives.service;

import and.digital.initiatives.exception.ErrorResponseCode;
import and.digital.initiatives.exception.InitiativeException;
import and.digital.initiatives.model.Initiative;
import and.digital.initiatives.repository.InitiativeRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class InitiativesService {
    @Autowired
    private InitiativeRepository repository;

    public List<Initiative> findIntiativesByTitle(String title) {
        return repository.findInitiativeByTitle(title);
    }

    public List<Initiative> findInitiativesByClub(String title) {
        return repository.findInitiativeByClub(title);
    }

    public List<Initiative> findInitiativesByCategory(String title) {
        return repository.findInitiativeByCategory(title);
    }

    public List<Initiative> findInitiativesByCategoryAndTitle(String category, String title) {
        return repository.findInitiativeByCategoryAndTitle(category, title);
    }

    public List<Initiative> findInitiativesByClubAndTitle(String club, String title) {
        return repository.findInitiativeByClubAndTitle(club, title);
    }

    public List<Initiative> findInitiativeBySearchTerm(String searchTerm) {
        return repository.findInitiativeByTitleContainingIgnoreCaseOrInitleadContainingIgnoreCaseOrClubIgnoreCaseContainingOrCategoryContainingIgnoreCaseOrDescriptionContainingIgnoreCase(searchTerm, searchTerm, searchTerm, searchTerm, searchTerm);
    }

    public List<Initiative> findInitiativeByClubAndSearchTerm(String club, String searchTerm) {
        return repository.findInitiativeByTitleContainingIgnoreCaseOrInitleadContainingIgnoreCaseOrClubIgnoreCaseContainingOrCategoryContainingIgnoreCaseOrDescriptionContainingIgnoreCase(searchTerm, searchTerm, searchTerm, searchTerm, searchTerm);
    }

    public List<Initiative> findInitiativeByCategoryAndSearchTerm(String category, String searchTerm) {
        return repository.findInitiativeByTitleContainingIgnoreCaseOrInitleadContainingIgnoreCaseOrClubIgnoreCaseContainingOrCategoryContainingIgnoreCaseOrDescriptionContainingIgnoreCase(searchTerm, searchTerm, searchTerm, searchTerm, searchTerm);
    }

    public Page<Initiative> findAll(Pageable pageable) {
        return repository.findAll(pageable);
    }

    public List<Initiative> findAll() {
        return repository.findAll();
    }

    public Initiative save(Initiative initiative) {
        List<Initiative> exists = findIntiativesByTitle(initiative.getTitle());
        if(exists != null && !exists.isEmpty()) {
            throw new InitiativeException(ErrorResponseCode.DUPLICATE_INITIATIVE,"Initiative title already exists!");
        }
        return repository.save(initiative);
    }
    public Initiative update(Initiative initiative) {
        List<Initiative> exists = findIntiativesByTitle(initiative.getTitle());
        Initiative found = exists.stream().filter(i -> i.getTitle().equalsIgnoreCase(initiative.getTitle()) && i.getId() != initiative.getId()).findAny().orElse(null);
        return repository.save(initiative);
    }


    public void delete(long id) {
        repository.deleteById(id);
    }

    public Optional<Initiative> findById(long id) {
        return repository.findById(id);
    }
}
