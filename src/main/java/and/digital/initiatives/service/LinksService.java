package and.digital.initiatives.service;

import and.digital.initiatives.exception.ErrorResponseCode;
import and.digital.initiatives.exception.LinkException;
import and.digital.initiatives.model.Link;
import and.digital.initiatives.repository.LinkRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class LinksService {
    @Autowired
    private LinkRepository repository;
    public List<Link> findAll() {
        return repository.findAll();
    }
    public Link findById(long id) {
        return repository.findById(id);
    }

    public Link save(Link link) {
        if(link == null) {
            throw new LinkException(ErrorResponseCode.INVALID_LINK,"Invalid Link");
        }
        return repository.save(link);
    }

    public void delete(Link link) {
        if(link == null) {
            throw new LinkException(ErrorResponseCode.INVALID_LINK,"Invalid Link");
        }
        repository.delete(link);
    }

}
