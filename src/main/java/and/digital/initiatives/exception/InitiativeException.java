package and.digital.initiatives.exception;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class InitiativeException extends RuntimeException {

    private  ErrorResponseCode errorCode;
    private  String message;

    public InitiativeException(ErrorResponseCode errorCode,  String message) {
        super(message);
        this.errorCode = errorCode;
        this.message = message;
    }
}
