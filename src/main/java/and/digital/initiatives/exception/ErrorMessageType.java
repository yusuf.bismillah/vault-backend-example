package and.digital.initiatives.exception;

public enum ErrorMessageType {
    // Update the id field later after agreement with front end to so that front end can
    // identify is  error message if has to show to user.
    BAD_REQUEST(1);
    int id;
    ErrorMessageType(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
       return String.valueOf(this.id);
    }

    public int getValue() {
        return this.id;
    }
}
