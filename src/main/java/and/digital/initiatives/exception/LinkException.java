package and.digital.initiatives.exception;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LinkException extends RuntimeException {

    private final ErrorResponseCode errorCode;

    public LinkException(final ErrorResponseCode errorCode, final String message) {
        super(message);
        this.errorCode = errorCode;
    }
}
