package and.digital.initiatives.exception;

public class ErrorMessage {
    private int id;
    private String message;
    public ErrorMessage(int errorId, String message) {
        this.id = errorId;
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
