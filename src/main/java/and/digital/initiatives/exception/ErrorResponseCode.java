package and.digital.initiatives.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

public enum ErrorResponseCode {
    INVALID_SEARCH_TYPE(HttpStatus.INTERNAL_SERVER_ERROR),
    DUPLICATE_INITIATIVE(HttpStatus.BAD_REQUEST),
    INVALID_INITIATIVE(HttpStatus.BAD_REQUEST),
    INVALID_LINK(HttpStatus.INTERNAL_SERVER_ERROR);

    @Getter
    HttpStatus httpStatus;

    ErrorResponseCode(final  HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }
}
