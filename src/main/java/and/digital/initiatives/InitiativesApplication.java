package and.digital.initiatives;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.config.BootstrapMode;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


//Main class which runs application
@SpringBootApplication
//@EnableAutoConfiguration
//@EnableJpaRepositories(bootstrapMode = BootstrapMode.DEFAULT)
public class InitiativesApplication  implements CommandLineRunner{

	@Value("${spring.datasource.driver-class-name}")
	private String driver;

	@Value("${okta.oauth2.issuer}")
	private String issuer;

	@Value("${spring.security.oauth2.resourceserver.jwt.issuer-uri}")
	private String jwtIssuer;

	public static void main(String[] args) {
		SpringApplication.run(InitiativesApplication.class, args);

	}

	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**").allowedOrigins("https://dev-the-vault.and-newton.co.uk","http://localhost:3000")
						.allowedMethods("GET", "POST", "DELETE", "PUT", "HEAD")
						.allowCredentials(true);
			}
		};
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("Param value is " + driver);
		System.out.println("Param value is " + issuer);
		System.out.println("Param value is " + jwtIssuer);
	}
}
