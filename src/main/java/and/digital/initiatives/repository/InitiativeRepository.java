package and.digital.initiatives.repository;

import and.digital.initiatives.model.Initiative;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

//Initiative repository used to establish connection between controller and database model
@Repository
public interface InitiativeRepository extends JpaRepository<Initiative, Long> {

    List<Initiative> findInitiativeByTitle(String title);
    List<Initiative> findInitiativeByCategory(String category);
    List<Initiative> findInitiativeByClub(String club);
    List<Initiative> findInitiativeByClubAndTitle(String club, String title);
    List<Initiative> findInitiativeByCategoryAndTitle(String category, String title);
    Page<Initiative> findAll(Pageable pageable);
    List<Initiative> findAll();
    List<Initiative> findInitiativeByTitleContainingIgnoreCaseOrInitleadContainingIgnoreCaseOrClubIgnoreCaseContainingOrCategoryContainingIgnoreCaseOrDescriptionContainingIgnoreCase(String title, String initLead, String club, String category, String description);
    List<Initiative> findInitiativeByClubAndTitleContainingIgnoreCaseOrInitleadContainingIgnoreCaseOrCategoryContainingIgnoreCaseOrDescriptionContainingIgnoreCase(String club, String title, String initLead, String category, String description);
    List<Initiative> findInitiativeByCategoryAndTitleContainingIgnoreCaseOrInitleadContainingIgnoreCaseOrClubContainingIgnoreCaseOrDescriptionContainingIgnoreCase(String category, String title, String initLead, String club, String description);

}
