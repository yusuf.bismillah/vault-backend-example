package and.digital.initiatives.repository;

import and.digital.initiatives.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
// add pagination for user after implemeting user integration by importing user csv file
public interface UserRepository extends JpaRepository<User, Long> {
    //List<User> findAllB
    @Query(value = "select u from User u" +
            " where u.firstName like %:searchterm%" +
            " OR " +
            "u.lastName LIKE %:searchterm% ")
    List<User> findUserBySearchterm(@Param("searchterm") String searchterm);

    @Query(value = "select u from User u" +
            " where u.firstName like %:firstname%" +
            " OR " +
            "u.lastName LIKE %:lastname% ")
    List<User> findUserByFirstNameLastName(@Param("firstname") String searchterm, @Param("lastname") String lastname);

    List<User> findUsersByEmail(String email);
}
