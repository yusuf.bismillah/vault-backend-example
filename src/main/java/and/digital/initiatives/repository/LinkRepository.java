package and.digital.initiatives.repository;

import and.digital.initiatives.model.Link;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LinkRepository extends JpaRepository<Link, Long> {
    List<Link> findAll();
    Link findById(long id);
}
