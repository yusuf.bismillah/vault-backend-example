package and.digital.initiatives.sorter;

import and.digital.initiatives.model.Initiative;

import java.util.Comparator;

public class InitiativesSortByDate implements Comparator<Initiative> {

    @Override
    public int compare(Initiative o1, Initiative o2) {
        return o1.getCreateddate().compareTo(o2.getCreateddate());
    }
}
