package and.digital.initiatives.sorter;

import and.digital.initiatives.model.User;

import java.util.Comparator;

public class UserSortByFirstName implements Comparator<User> {
    @Override
    public int compare(User o1, User o2) {
        return o1.getFirstName().compareTo(o2.getFirstName());
    }
}
