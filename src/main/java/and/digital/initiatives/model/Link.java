package and.digital.initiatives.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "LINKS")
//@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Link {

    @Id
    //@GeneratedValue(strategy = GenerationType.TABLE)
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;

    @Column(length = 150)
    private String type;

    @Column(length = 2083)
    @NotNull
    private String link;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonBackReference("initiative-links")
    private Initiative initiative;

    @Override
    public String toString() {
        return String.valueOf(this.getId());
    }

}