package and.digital.initiatives.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "INITIATIVES")
//@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Initiative {

    @Id
    //@GeneratedValue(strategy = GenerationType.TABLE)
   // @GeneratedValue(strategy = GenerationType.AUTO)
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;

    @Column(length = 50)
    private String title;

    @Column(nullable = false)
    @NotNull
    private String description;

    @Column(nullable = false)
    //@NotNull
    private String initlead;

    @Column(nullable = false)
    @NotNull
    private String category;

    @Column(nullable = false)
    @NotNull
    private String club;

    @Column(columnDefinition = "BOOLEAN NOT NULL DEFAULT TRUE")
    private boolean active;

    @Column
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone="Europe/London")
    private LocalDate createddate;

    @Transient
    private int searchMatches;

    public boolean getActive() {
    	return active;
    }
    // cascade all has been commented as it is not required because links are added or deleted separately by link api
    // in future change or remove the link api and do all operation of adding or removing the link
    // through initiative update or add new api path in initiative controller to update only link delete/add and uncomment the cascade all.
    @OneToMany(/*cascade={CascadeType.ALL},*/ fetch = FetchType.EAGER)
    @JoinColumn(name="initiative_id", referencedColumnName = "id")
    @JsonManagedReference("initiative-links")
    //@JsonIgnore
    private Set<Link> links = new HashSet<Link>();

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "initiative_lead",
              joinColumns = {@JoinColumn(name = "inititative_id", referencedColumnName = "id")},
              inverseJoinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")}
            )
    //@JsonManagedReference
    private Set<User> leads = new HashSet<>();

    @Override
    public int hashCode() {
        return (int)this.getId();
    }

    @Override
    public String toString() {
        return String.valueOf(this.getId());
    }

    
}