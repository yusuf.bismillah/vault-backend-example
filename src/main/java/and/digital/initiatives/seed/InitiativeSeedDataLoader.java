package and.digital.initiatives.seed;

import and.digital.initiatives.repository.InitiativeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;

//Dummy seed data used for testing
@Component
public class InitiativeSeedDataLoader implements ApplicationRunner {

    private InitiativeRepository initiativeRepository;
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

    //placeholders
    String whereAndiDate = "12-04-2018";
    String scheduleDashboardDate = "04-03-2019";
    String graphQLDate = "10-05-2019";
    String socialDate = "16-09-2019";
    String reachAndOpsDate = "15-02-2019";
    String techNewsDate = "25-11-2018";
    String genderDivDate = "08-03-2017";
    String bameDate = "17-11-2017";
    String plantTreesDate = "01-09-2018";
    String lgbtDate = "23-05-2017";
    String sandBoxDate = "04-10-2019";
    String leadingSquadsDate = "19-07-2018";
    String brandBuildingDate = "25-01-2019";
    String cultureExplorationDate = "21-07-2018";


    @Autowired
    public InitiativeSeedDataLoader(InitiativeRepository initiativeRepository) {
        this.initiativeRepository = initiativeRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
//        initiativeRepository.save(Initiative.builder()
//                .title("Where’s my Andi?")
//                .description("Club Turing aims to build an api service that can identify where an Andis(physical "
//                		+ "location and what client they been on each day). This will help facilitate easier "
//                		+ "communication and transparency with AND. Auto-Completion on Elapse-timesheet.")
//                .initlead("Bogdan, Stefan, Tony")
//                .club("Turing")
//                .category("AND Tools")
//                .active(true)
//                .createddate(LocalDate.parse(whereAndiDate, formatter))
//                .build());
//        initiativeRepository.save(Initiative.builder()
//                .title("Scheduling Dashboard")
//                .description("Club Turing aims to create a scheduling dashboard for employees at AND. This will "
////                		+ "provide visibility of current and upcoming client work to all ANDis company-wide, "
////                		+ "cross-club collaboration as well as company-wide access to codebase provides "
//                		+ "individual clubs to tailor their dashboards to their own needs.")
//                .initlead("Adam, Philippa, Sharn, Elias")
//                .club("Turing")
//                .category("AND Tools")
//                .active(true)
//                .createddate(LocalDate.parse(scheduleDashboardDate, formatter))
//                .build());
//        initiativeRepository.save(Initiative.builder()
//                .title("GraphQL Security Exploits Exercise")
//                .description("We wish to extend the exercises part of the web app security to include a working "
////                		+ "playground for graphql security exploits - Build DoS attack Use Case."
////                		+ "This will help update the existing Web App Security training course with modern technologies "
//                		+ "so that andis can have a better understanding of security vulnerabilities in GraphQL.")
//                .initlead("Bogdan & Mamta")
//                .club("Turing")
//                .category("Learning")
//                .active(true)
//                .createddate(LocalDate.parse(graphQLDate, formatter))
//                .build());
//        initiativeRepository.save(Initiative.builder()
//                .title("AND Social")
//                .description("Across AND we spend a lot on social budget for our ANDis. There’s a lot of repetition "
////                		+ "across Clubs & Squads, and it would be great if there was some way of finding recommended "
////                		+ "locations for squad and club activity. This could include basic info like address, type of "
////                		+ "social etc, but also incorporate reviews, comments etc. By building AND Social, we will "
//                		+ "provide a company-wide scheduling and budgeting tool for socials and events.")
//                .initlead("N/A")
//                .club("Newton")
//                .category("AND Tools")
//                .active(false)
//                .createddate(LocalDate.parse(socialDate, formatter))
//                .build());
//        initiativeRepository.save(Initiative.builder()
//                .title("React and Dev Ops Upskilling")
//                .description("We aim to upskill and research the tech stack of the AND website rebuild. And also to "
////                		+ "better understand Gatsby, React, and Contentful to deliver an industry-quality website. "
////                		+ "This will aid in creating a new vision and brand image for the company and draw in the "
//                		+ "required audience specified by Marketing and Recruiting.")
//                .initlead("Aviraj & Chris")
//                .club("Turing")
//                .category("Learning")
//                .active(true)
//                .createddate(LocalDate.parse(reachAndOpsDate, formatter))
//                .build());
//        initiativeRepository.save(Initiative.builder()
//                .title("Turing Tech News App")
//                .description("We aim to develop a PWA tech news service (app) using front and back end javascript technologies. "
////                		+ "Our MVP is to design and implement a working news feed service which filters important news - chosen "
////                		+ "by ANDis - by category that is relevant to AND Digital and its ANDis.\n" +
////                		"\n" +
////                		"After MVP, there is an opportunity for further expansion, for example, to feed into the dashboard or "
////                		+ "slack at Turing allowing further user interaction.\n" +
////                		"\n" +
////                		"The completed tool will enable greater awareness of technical advancements and topics within and "
//                		+ "external to Turing helping to foster innovation")
//                .initlead("Arjun, Avi, Chris & Elias")
//                .club("Turing")
//                .category("AND Tools")
//                .active(true)
//                .createddate(LocalDate.parse(techNewsDate, formatter))
//                .build());
//        initiativeRepository.save(Initiative.builder()
//                .title("Gender Diversity")
//                .description("At Kilburn we believe in equality in the workspace. Women account for 31% of Andis. We would "
////                		+ "like to improve our gender balance by 20%, increase females in initinitleadership roles by 10%, build a "
////                		+ "reputation and be recognised as being a good employer for women. To achieve this we will "
//                		+ "support external recruitment efforts, educate on gender basis, support Marketing with female-focused marketing.")
//                .initlead("Maz Hussain, Neil Pieprzak, Mallar Dasgupta")
//                .club("Dekker")
//                .category("Diversity")
//                .active(true)
//                .createddate(LocalDate.parse(genderDivDate, formatter))
//                .build());
//        initiativeRepository.save(Initiative.builder()
//                .title("BAME Diversity")
//                .description("At Kilburn we believe in equality in the workspace. 29% of ANDis come from a BAME background. "
////                		+ "24% of BAME ANDIs are in a leadership role. We aim to Increase our representation across the business, "
////                		+ "celebrate our diverse backgrounds, ensure cultural and ethnic understanding and sensitivity across AND. "
////                		+ "To that end we will incorporate cultural and ethnic sensitivity and or unconscious bias training as "
////                		+ "part of bootcamp, celebrations at workplaces to promote inclusiveness. We will also ask about creating "
//                		+ "a learning week to bring awareness to BAME leads or innovators in tech.")
//                .initlead("Mallar Dasgupta")
//                .club("Kilburn")
//                .category("Diversity")
//                .active(true)
//                .createddate(LocalDate.parse(bameDate, formatter))
//                .build());
//        initiativeRepository.save(Initiative.builder()
//                .title("Plant trees")
//                .description("At one of our clubs, Dekker, we are running a tree planting initiative where for every 100 story points "
////                		+ "completed across a number of projects we plant 1 tree (At AND we work following the Agile framework). "
////                		+ "We are partnering with a UK based charity, Trees for Cities, in order to plant these trees. "
////                		+ "This initiative is embedded in our ways of working so does not require any additional work from our "
////                		+ "employees, leading to greater engagement with the process as it is a byproduct of their hard work. "
////                		+ "Monthly updates are given, including estimates of the amount of CO2 offset by the trees planted. "
////                		+ "This is done using everyday activities as a comparison, such as, number of coffees or boils of a kettle.\n" +
////                		"Currently this initiative is running until mid December when the trees will be planted.\n" +
////                		"However, we will be kick-starting the idea once again in the new year but expanding this across a "
////                		+ "larger number of projects. The long term aim is to partner with our clients on this too in order "
//                		+ "to make the biggest impact possible.")
//                .initlead("Bijan")
//                .club("Dekker")
//                .category("Giving back")
//                .active(true)
//                .createddate(LocalDate.parse(plantTreesDate, formatter))
//                .build());
//        initiativeRepository.save(Initiative.builder()
//                .title("LGBT+ Diversity")
//                .description("Current emphasis on support for ‘coming-out’ in the workplace. We aim to to ensure an inclusive "
////                		+ "environment for those within the LGBT community, so they can bring their authentic selves to AND, "
////                		+ "help non-LGBT+ people participate in building this environment. The end goal is to be able to "
//                		+ "develop a strong and healthy inclusive community at AND, and be recognised as such an organisation.")
//                .initlead("Mark Faulkner")
//                .club("Kilburn")
//                .category("Diversity")
//                .active(true)
//                .createddate(LocalDate.parse(lgbtDate, formatter))
//                .build());
//        initiativeRepository.save(Initiative.builder()
//                .title("SANDbox")
//                .description("Each Club or Business Unit have various initiatives active at any one time, we often have "
////                		+ "duplication without any knowledge. At club Newton, we aim to create a place where everybody can "
////                		+ "see all the initiatives. This web-based tool will allow Andis to share ideas, collaborate and w"
//                		+ "ill encourage the growth of all company-wide initiatives.")
//                .initlead("N/A")
//                .club("Newton")
//                .category("AND Tools")
//                .active(false)
//                .createddate(LocalDate.parse(sandBoxDate, formatter))
//                .build());
//        initiativeRepository.save(Initiative.builder()
//                .title("Leading squads")
//                .description("At Ada we are focused on improving Andis work experience. Thus, we will be hosting regular"
////                		+ "sessions in house to improve the knowledge and skillsets of our squad leads. These will be both"
////                		+ "technical and social learning exercises which will hopefully improve the employee exepreicne here at"
//                		+ "AND")
//                .initlead("N/A")
//                .club("Ada")
//                .category("Wellbeing")
//                .active(false)
//                .createddate(LocalDate.parse(leadingSquadsDate, formatter))
//                .build());
//        initiativeRepository.save(Initiative.builder()
//                .title("Brand building workshops")
//                .description("At the moment brand building is a vague concept for most Andis. Thus, at Tenzing, we be hosting workshops"
////                		+ "on general brand building with AND's key values in mind. This will hopefully allow all of us to be able to "
//                		+ "better understand the company and build its brand internationally.")
//                .initlead("Mark Faulkner")
//                .club("Tenzing")
//                .category("AND Brand")
//                .active(true)
//                .createddate(LocalDate.parse(brandBuildingDate, formatter))
//                .build());
//        initiativeRepository.save(Initiative.builder()
//                .title("Culture exploration")
//                .description("At Murray, we believe that there is real value in exploring different and varying cultures. To that end "
////                		+ "we will be hosting differently themed events once a month. These will include food, history and clothing prepared"
////                		+ "by representatives of said culture. This will allow us to establish lasting connections and shared understanding "
//                		+ "between Andis of different backgrounds")
//                .initlead("N/A")
//                .club("Murray")
//                .category("Culture")
//                .active(false)
//                .createddate(LocalDate.parse(cultureExplorationDate, formatter))
//                .build());
//
        }
}
