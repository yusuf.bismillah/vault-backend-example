var QUERY_STRING_ID = "id";
var searchText = "";
const baseUri = "/api/initiatives/";

$(document).ready(function() {
  function truncateString(str, length) {
    return str.length > length ? str.substring(0, length - 3) + "..." : str;
  }

  loadInitiative();

  function loadInitiative() {
    $.ajax({
      type: "GET",
      contentType: "application/json",
      url: baseUri,
      dataType: "json",
      beforeSend: function() {
        $(".spinner-border").show();
      },
      success: function(data) {
        var initiative_data = "";
        $.each(data, function(key, value) {
          var truncatedDescription = truncateString(value.description, 100);
          initiative_data += '<tr class="tableRow">';
          initiative_data +=
            '<td class="pt-3 pb-3" attr-id="' +
            value.id +
            '"> <b class="text-primary">' +
            value.title +
            "</b> <br/> <small>" +
            truncatedDescription +
            "</small></td>";
          initiative_data += "</tr>";
        });

        $("#initiativeList").append(initiative_data);
      },
      error: function(e) {
        if (e.status == "0") {
          showInitiativeTableNoResultsMessage();
        } else {
          displayGeneralErrorPage();
        }
      },
      complete: function() {
        $(".spinner-border").hide();
      }
    });
  }

  $("#no-results").hide();

  $("#searchInitiative").keyup(function() {
    clearTimeout($.data(this, "timer"));
    if ($(this).val().length > 0) {
      var wait = setTimeout(search, 500);
    } else if ($(this).val().length === 0) {
      loadInitiative();
    }
    $(this).data("timer", wait);
    searchText = $(this).val();
  });

  function search() {
    var resultCount = 0;
    $.ajax({
      type: "GET",
      contentType: "application/json",
      url: baseUri + "search?searchTerm=" + searchText,
      dataType: "json",
      beforeSend: function() {
        $(".spinner-border").show();
      },
      success: function(data) {
        if (data.length > 0) {
          resultCount++;
          $("#initiativeList").empty();
          var initiative_data = "";
          $.each(data, function(key, value) {
            var truncatedDescription = truncateString(value.description, 100);
            initiative_data += '<tr class="tableRow">';
            initiative_data +=
              '<td class="pt-3 pb-3" attr-id="' +
              value.id +
              '"> <b class="text-primary">' +
              value.title +
              "</b> <br/> <small>" +
              truncatedDescription +
              "</small></td>";
            initiative_data += "</tr>";
          });

          $("#initiativeList").append(initiative_data);
        }
        if (resultCount == 0) {
          showInitiativeTableNoResultsMessage();
        } else {
          $("#no-results").hide();
          $("#initiative-container").show();
        }
      },
      error: function(e) {
        if (e.status == "0") {
          showInitiativeTableNoResultsMessage();
        } else {
          displayGeneralErrorPage();
        }
      },
      complete: function() {
        $(".spinner-border").hide();
      }
    });
  }

  $("#initiativeList").on("click", "td", function() {
    var selectedInitiative = $(this);

    var selectedInitiativeId = selectedInitiative.attr("attr-id");

    viewInitiative(selectedInitiativeId);
  });

  function viewInitiative(initiativeId) {
    window.location.href =
      "../views/viewInitiative.html?" + QUERY_STRING_ID + "=" + initiativeId;
  }

  $("#createSubmit").click(function() {
    var initiativeTitle = $("#initTitle").val();
    var initiativeDescription = $("#initiativeDescription").val();
    var initiativeOwner = $("#initiativeOwner").val();
    var initiativeClub = $("#initiativeClub").val();
    var initiativeCategory = $("#initiativeCategory").val();
    var initiativeIsActive = false;

    if ($("#statusCheck").is(":checked")) {
      initiativeIsActive = true;
    }

    $("#form").validate({
      rules: {
        initTitle: {
          required: true,
          minlength: 3,
          maxlength: 30
        },
        initiativeDescription: {
          required: true,
          maxlength: 1000
        },
        initiativeOwner: {
          required: true
        },
        initiativeClub: {
          required: true
        },
        initiativeCategory: {
          required: true
        }
      }
    });

    var msg = {
      title: initiativeTitle,
      description: initiativeDescription,
      initlead: initiativeOwner,
      category: initiativeCategory,
      club: initiativeClub,
      active: initiativeIsActive
    };

    if ($("#form").valid()) {
      var jsonMsg = JSON.stringify(msg);
      $.ajax({
        type: "POST",
        contentType: "application/json",
        url: baseUri,
        dataType: "json",
        data: jsonMsg,
        beforeSend: function() {
          $(".spinner-border").show();
        },
        success: function(data) {
          var initiativeId = data.id;
          viewInitiative(initiativeId);
        },
        error: function(e) {
          /// We should add some useful errors for the user to see depenind on the status code
          displayGeneralErrorPage();
        },
        complete: function() {
          $(".spinner-border").hide();
        }
      });
    }
  });

  function showInitiativeTableNoResultsMessage() {
    $("#no-results").show();
    $("#initiative-container").hide();
  }

  function displayGeneralErrorPage() {
    window.location.href = "../views/generalError.html";
  }
});
