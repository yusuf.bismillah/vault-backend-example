
$(document).ready(function(){
        
    var initiativeId = getUrlVars("id")
    var getInitiativeByIdUrl = "/api/initiatives/" + initiativeId

    $.get(getInitiativeByIdUrl, function(data) {
        $("#title").html(data.title)
        $("#description").html(data.description)
    });

    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: getInitiativeByIdUrl,
        dataType: 'json',
        beforeSend: function() {
            $(".spinner-border").show();
        },
        success: function(data) {
            $("#title").html(data.title)
            $("#description").html(data.description)
            $("#lead").html(data.initlead)
            $("#category").html(data.category)
            $("#club").html(data.club)

            if(data.active == true) {
                $("#status").addClass("text-success")
                $("#status").removeClass("text-danger")
                $("#status").html("Active <i class='fa fa-check-circle'></i>")
            } else {
                $("#status").addClass("text-danger")
                $("#status").removeClass("text-success")
                $("#status").html("Inactive <i class='fa fa-minus-circle'></i>")
            }
        },
        error: function(e) {
            displayGeneralErrorPage()
        },
        complete: function() {
            $(".spinner-border").toggle();
        }
    });

    function getUrlVars(QueryStringParam)
    {
        var url_string = window.location.href;
        var url = new URL(url_string);
        var initiativeId = url.searchParams.get(QueryStringParam);

        return initiativeId
    }

    function displayGeneralErrorPage() {
        window.location.href = '../views/generalError.html';
    }
}
);