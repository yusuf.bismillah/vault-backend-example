package and.digital.initiatives.controller;

import and.digital.initiatives.model.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.jayway.jsonpath.JsonPath;
import net.minidev.json.JSONArray;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest()
@ActiveProfiles("test")
@AutoConfigureMockMvc(addFilters = false)
@Ignore
public class UserControllerTest {
    @Autowired
    private MockMvc mvc;

    private static final String FIRST_NAME = "Test";
    private static final String LAST_NAME = "User";
    private static final String CLUB = "Newton";
    private static final String SQUAD = "Prometheus";
    private static final String EMAIL = "test@and.digital";
    private static final String ADMIN_HEADER = "requester";
    private static final String ADMIN_UUID = "e4cde203-ddc2-4798-8449-4c59f71171e7";
    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    // Writing all test cases in one as test user is deleted and api generate random id.
    @Test
    public void createUpdateGetDeleteUser() throws Exception {
        //Create User
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post("/api/initiatives/users")
                .contentType("application/json")
                .accept("application/json")
                .header(ADMIN_HEADER, ADMIN_UUID)
                .content(makeMapper().writeValueAsString(buildUserWithoutId()))
        ).andDo(print())
                .andExpect(status().isCreated())
                .andReturn();
        ResultActions resultActions = mvc.perform(MockMvcRequestBuilders.get("/api/initiatives/search?searchTerm=Test title"))
                .andDo(print())
                .andExpect(status().isOk());
        MvcResult result = resultActions.andReturn();
        JSONArray array = JsonPath.read(mvcResult.getResponse().getContentAsString(),"$..id");

        // Update User
        mvcResult = mvc.perform(MockMvcRequestBuilders.put("/api/initiatives/users/" + array.get(0))
                .contentType("application/json")
                .accept("application/json")
                .header(ADMIN_HEADER, ADMIN_UUID)
                .content(makeMapper().writeValueAsString(buildUserWitId(Long.valueOf(array.get(0).toString()))))
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        // Get User
        mvcResult = mvc.perform(MockMvcRequestBuilders.get("/api/initiatives/users/" + array.get(0))
                .contentType("application/json")
                .accept("application/json")
                .header(ADMIN_HEADER, ADMIN_UUID)
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        // Seacrh User
        mvcResult = mvc.perform(MockMvcRequestBuilders.get("/api/initiatives/users/search?searchTerm=" + FIRST_NAME)
                .contentType("application/json")
                .accept("application/json")
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        // Delete User
        mvcResult = mvc.perform(MockMvcRequestBuilders.delete("/api/initiatives/users/" + array.get(0))
                .header(ADMIN_HEADER, ADMIN_UUID)
                .accept("application/json")
                .content(makeMapper().writeValueAsString(buildUserWithoutId()))
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
    }

    private User buildUserWithoutId() {
        return User.builder()
                .firstName(FIRST_NAME)
                .lastName(LAST_NAME)
                .email(EMAIL)
                .club(CLUB)
                .squad(SQUAD)
                .build();
    }

    private User buildUserWitId(Long id) {
        return User.builder()
                .id(id)
                .firstName(FIRST_NAME)
                .lastName(LAST_NAME)
                .email(EMAIL)
                .club(CLUB)
                .squad(SQUAD)
                .build();
    }

    private ObjectMapper makeMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new ParameterNamesModule());
        mapper.registerModule(new Jdk8Module());
        mapper.registerModule(new JavaTimeModule());
        return mapper;
    }
}