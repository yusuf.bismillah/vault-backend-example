package and.digital.initiatives.controller;

import and.digital.initiatives.model.Initiative;
import and.digital.initiatives.model.Link;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.jayway.jsonpath.JsonPath;
import net.minidev.json.JSONArray;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest()
@AutoConfigureMockMvc(addFilters = false)
@ActiveProfiles("test")
@Ignore
public class LinksControllerIT {

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Autowired
    private MockMvc mvc;

    private static final String VALID_CLUB_NAME = "newton";
    @Test
    public void testListAll() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/api/initiatives/links"))
                .andDo(print())
                .andExpect(status().isOk());
    }


    @Test
    public void testCreateListDelete() throws Exception {
        //first check if inititative by test title already exist then delete it
        ResultActions testActions = mvc.perform(MockMvcRequestBuilders.get("/api/initiatives/search?searchTerm=Test title"))
                .andDo(print());
        MvcResult testResult = testActions.andReturn();
        JSONArray resultArray = JsonPath.read(testResult.getResponse().getContentAsString(),"$..id");
        if(!resultArray.isEmpty()) {
            mvc.perform(MockMvcRequestBuilders.delete("/api/initiatives/"+resultArray.get(0)+"/links"))
                    .andDo(print())
                    .andExpect(status().isOk());
        }



        //Create an Initiative
        mvc.perform(MockMvcRequestBuilders.post("/api/initiatives")
                .contentType(MediaType.APPLICATION_JSON)
                .content(makeMapper().writeValueAsString(getTestInititiveData())))
                .andDo(print())
                .andExpect(status().isCreated());

        //Fetch the id
        ResultActions resultActions = mvc.perform(MockMvcRequestBuilders.get("/api/initiatives/search?searchTerm=Test title"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$..title").value("Test title"));
        MvcResult result = resultActions.andReturn();
        JSONArray array = JsonPath.read(result.getResponse().getContentAsString(),"$..id");

        //Create a link for the Initiative
        mvc.perform(MockMvcRequestBuilders.post("/api/initiatives/"+array.get(0)+"/links")
                .contentType(MediaType.APPLICATION_JSON)
                .content(makeMapper().writeValueAsString(getTestLinkData())))
                .andDo(print())
                .andExpect(status().isCreated());

        //Delete the link for the initiative
        mvc.perform(MockMvcRequestBuilders.delete("/api/initiatives/"+array.get(0)+"/links"))
                .andDo(print())
                .andExpect(status().isOk());

        //Delete the initiative
        mvc.perform(MockMvcRequestBuilders.delete("/api/initiatives/"+array.get(0)))
                .andDo(print())
                .andExpect(status().isOk());
    }

    private Link getTestLinkData() {
        return Link.builder()
                .link("test link")
                .type("test type")
                .initiative(null)
                .build();
    }

    private Initiative getTestInititiveData() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return Initiative.builder()
                .title("Test title")
                .category("AND Tools")
                .club(VALID_CLUB_NAME)
                .initlead("Marty Mcfly")
                .active(true)
                .description("This is very descriptive!")
                .createddate(LocalDate.parse("28-10-2018", formatter))
                .build();
    }

    private ObjectMapper makeMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new ParameterNamesModule());
        mapper.registerModule(new Jdk8Module());
        mapper.registerModule(new JavaTimeModule());
        return mapper;
    }
}
