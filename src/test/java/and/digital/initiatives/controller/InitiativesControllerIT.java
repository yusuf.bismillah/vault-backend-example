package and.digital.initiatives.controller;


import and.digital.initiatives.model.Initiative;
import and.digital.initiatives.service.InitiativesService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.jayway.jsonpath.JsonPath;
import net.minidev.json.JSONArray;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.request;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest()
@ActiveProfiles("test")
@AutoConfigureMockMvc(addFilters = false)
@Ignore
public class InitiativesControllerIT {

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Autowired
    private MockMvc mvc;

    private int initiativeId;

    private static final String VALID_CLUB_NAME = "newton";
    private static final String INVALID_CLUB_NAME = "something";

    @Before
    public void setUp() throws Exception {
        //check if initiaitve by test tile exist, if it exist delete it before executing test

        ResultActions resultActions = mvc.perform(MockMvcRequestBuilders.get("/api/initiatives/search?searchTerm=Test title"))
                .andDo(print());
        MvcResult result = resultActions.andReturn();
        JSONArray array = JsonPath.read(result.getResponse().getContentAsString(),"$..id");

        if(!array.isEmpty()) {
            mvc.perform(MockMvcRequestBuilders.delete("/api/initiatives/"+array.get(0)))
                    .andDo(print());
        }

    }

    @Test
    public void createUpdateGetIntiative() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post("/api/initiatives")
                .contentType("application/json")
                .accept("application/json")
                .content(makeMapper().writeValueAsString(getTestInititiveData()))
        )
                .andDo(print())
                .andExpect(status().isCreated())
        .andReturn();
        JSONArray array = JsonPath.read(mvcResult.getResponse().getContentAsString(),"$..id");
        if (!array.isEmpty()) {
            this.initiativeId = (Integer)array.get(0);
        }
        mvc.perform(MockMvcRequestBuilders.put("/api/initiatives/" + this.initiativeId)
                .contentType("application/json")
                .accept("application/json")
                .content(makeMapper().writeValueAsString(getTestInititiveData()))
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        mvc.perform(MockMvcRequestBuilders.get("/api/initiatives/" + initiativeId))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void createIntiativeWithoutLead() throws Exception {
        Initiative initiative = getTestInititiveData();
        initiative.setInitlead(null);
        //InitLead is required so null value for initlead retrun http 400
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post("/api/initiatives")
                .contentType("application/json")
                .accept("application/json")
                .content(makeMapper().writeValueAsString(initiative))
        )
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    public void createIntiativeWithoutClub() throws Exception {
        Initiative initiative = getTestInititiveData();
        initiative.setClub(null);
        //Club is required so null value for initlead retrun http 400
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post("/api/initiatives")
                .contentType("application/json")
                .accept("application/json")
                .content(makeMapper().writeValueAsString(initiative))
        )
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();
    }
    @Test
    public void createIntiativeWithoutCategory() throws Exception {
        Initiative initiative = getTestInititiveData();
        initiative.setCategory(null);
        //Category is required so null value for initlead retrun http 400
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post("/api/initiatives")
                .contentType("application/json")
                .accept("application/json")
                .content(makeMapper().writeValueAsString(initiative))
        )
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    public void testListAllWithPage() throws Exception {
        mvc.perform(request(HttpMethod.GET, "/api/initiatives?page=1"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void testListAll() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/api/initiatives/all"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void testSearchByTitle() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/api/initiatives/search?searchTerm=Test title"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void testSearchByClub() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/api/initiatives/club/" +  VALID_CLUB_NAME + "?page=1"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void testSearchByClubAndTitle() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/api/initiatives/club/" + VALID_CLUB_NAME + "/search?searchTerm=title"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void testSearchByClubAndTitleForInvalidClubName() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/api/initiatives/club/" + INVALID_CLUB_NAME + "/search?searchTerm=title"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testSearchByCategory() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/api/initiatives/category/categoryname/search?searchTerm=title"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void testSearchByCategoryAndTitle() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/api/initiatives/category/categoryname?page=1"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    //Creating the record, getting the id, updating the record and deleting the same
    // to make sure date is clean
    @Test
    public void testCreateUpdateAndDelete() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/api/initiatives")
                .contentType(MediaType.APPLICATION_JSON)
                .content(makeMapper().writeValueAsString(getTestInititiveData())))
                .andDo(print())
                .andExpect(status().isCreated());

        ResultActions resultActions = mvc.perform(MockMvcRequestBuilders.get("/api/initiatives/search?searchTerm=Test title"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$..title").value("Test title"));
        MvcResult result = resultActions.andReturn();
        JSONArray array = JsonPath.read(result.getResponse().getContentAsString(),"$..id");

        Initiative updated = getTestInititiveData();
        updated.setId((Integer)array.get(0));
        updated.setActive(false);
        mvc.perform(MockMvcRequestBuilders.put("/api/initiatives/"+array.get(0))
                .contentType(MediaType.APPLICATION_JSON)
                .content(makeMapper().writeValueAsString(updated)))
                .andDo(print())
                .andExpect(status().isOk());

        mvc.perform(MockMvcRequestBuilders.get("/api/initiatives/search?searchTerm=Test title"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$..active").value(false));

        mvc.perform(MockMvcRequestBuilders.delete("/api/initiatives/"+array.get(0)))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteInvalidInitiative() throws Exception {
        mvc.perform(MockMvcRequestBuilders.delete("/api/initiatives/invalid"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    private Initiative getTestInititiveData() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return Initiative.builder()
                .title("Test title")
                .category("AND Tools")
                .club(VALID_CLUB_NAME)
                .initlead("Marty Mcfly")
                .active(true)
                .description("This is very descriptive!")
                .createddate(LocalDate.parse("28-10-2018", formatter))
                .leads(new HashSet<>())
                .build();
    }

    private ObjectMapper makeMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new ParameterNamesModule());
        mapper.registerModule(new Jdk8Module());
        mapper.registerModule(new JavaTimeModule());
        return mapper;
    }
}
