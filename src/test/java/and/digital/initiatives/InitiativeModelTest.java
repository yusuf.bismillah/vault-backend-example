package and.digital.initiatives;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import and.digital.initiatives.model.Initiative;
import and.digital.initiatives.repository.InitiativeRepository;

//TODO check and remove later
@RunWith(SpringRunner.class)
@DataJpaTest
@Ignore
public class InitiativeModelTest {
 
    @Autowired
    private TestEntityManager entityManager;
 
    @Autowired
    private InitiativeRepository initiativeRepository;
    
    private Initiative testInitiative;
 
    @Before
    public void setUp() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    	
    	testInitiative = Initiative.builder()
                .title("Test Initiative Title 123")
                .category("AND Tools")
                .club("Dekker")
                .initlead("Marty Mcfly")
                .active(true)
                .description("This is very descriptive!")
                .createddate(LocalDate.parse("11-12-2018", formatter))
                .build();
    	
    	entityManager.persist(testInitiative);
        entityManager.flush();
    }

    @Test
    public void testGetInitiativeInfo() {
    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    	
    	String title = testInitiative.getTitle();
    	String category = testInitiative.getCategory();
    	String club = testInitiative.getClub();
    	String initlead = testInitiative.getInitlead();
    	boolean active = testInitiative.getActive();
    	String description = testInitiative.getDescription();
    	LocalDate createddate = testInitiative.getCreateddate();
    	
    
    	Assert.assertTrue(title == "Test Initiative Title 123");
    	Assert.assertTrue(category == "AND Tools");
    	Assert.assertTrue(club == "Dekker");
    	Assert.assertTrue(initlead == "Marty Mcfly");
    	Assert.assertTrue(active);
    	Assert.assertTrue(description == "This is very descriptive!");
    	Assert.assertTrue(createddate.equals(LocalDate.parse("11-12-2018", formatter)));
    	
    }
    
    @After
    public void tearDown() {
    	entityManager.remove(testInitiative);
        entityManager.flush();
    }
}