package and.digital.initiatives.service;

import and.digital.initiatives.exception.InitiativeException;
import and.digital.initiatives.model.Initiative;
import and.digital.initiatives.repository.InitiativeRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.lessThan;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class InitiativesServiceTest {

    private InitiativesService service;
    private static final String CLUB = "dekker";
    private static final String CATEGORY = "learning";
    private static final String SEARCH_TERM = "something";

    InitiativeRepository repository = mock(InitiativeRepository.class);

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Before
    public void setup() {
        service = spy(new InitiativesService(repository));
    }

    @Test
    public void findIntiativesByTitle() {
        when(repository.findInitiativeByTitle(anyString())).thenReturn(Arrays.asList(getTestInititiveData()));
        List<Initiative> result = service.findIntiativesByTitle("some title");
        assertResult(result);
    }

    @Test
    public void findIntiativesByTitleWhenNullIsPassed() {
        List<Initiative> result = service.findIntiativesByTitle(null);
        assertResultsForInvalidValues(result);
    }

    @Test
    public void findIntiativesByTitleWhenSpecialCharacterIsPassed() {
        List<Initiative> result = service.findIntiativesByTitle("%");
        assertResultsForInvalidValues(result);
    }

    @Test
    public void findInitiativesByClub() {
        when(repository.findInitiativeByClub(anyString())).thenReturn(Arrays.asList(getTestInititiveData()));
        List<Initiative> result = service.findInitiativesByClub("some club");
        assertResult(result);
    }

    @Test
    public void findInitiativesByClubWhenNullIsPassed() {
        List<Initiative> result = service.findInitiativesByClub(null);
        assertResultsForInvalidValues(result);
    }

    @Test
    public void findInitiativesByClubWhenSpecialCharacterIsPassed() {
        List<Initiative> result = service.findInitiativesByClub("*");
        assertResultsForInvalidValues(result);
    }

    @Test
    public void findInitiativesByCategory() {
        when(repository.findInitiativeByCategory(anyString())).thenReturn(Arrays.asList(getTestInititiveData()));
        List<Initiative> result = service.findInitiativesByCategory("some category");
        assertResult(result);
    }

    @Test
    public void findInitiativesByCategoryWhenNullIsPassed() {
        List<Initiative> result = service.findInitiativesByCategory(null);
        assertResultsForInvalidValues(result);
    }

    @Test
    public void findInitiativesByCategoryWhenSpecialCharacterIsPassed() {
        List<Initiative> result = service.findInitiativesByCategory("&");
        assertResultsForInvalidValues(result);
    }

    @Test
    public void findInitiativesByCategoryAndTitle() {
        when(repository.findInitiativeByCategoryAndTitle(anyString(), anyString())).thenReturn(Arrays.asList(getTestInititiveData()));
        List<Initiative> result = service.findInitiativesByCategoryAndTitle("some category","some title");
        assertResult(result);
    }

    @Test
    public void findInitiativesByCategoryAndTitleWhenNullCategoryIsPassed() {
        List<Initiative> result = service.findInitiativesByCategoryAndTitle(null,"some title");
        assertResultsForInvalidValues(result);
    }

    @Test
    public void findInitiativesByCategoryAndTitleWhenNullTitleIsPassed() {
        List<Initiative> result = service.findInitiativesByCategoryAndTitle("some category",null);
        assertResultsForInvalidValues(result);
    }

    @Test
    public void findInitiativesByCategoryAndTitleWhenNullIsPassed() {
        List<Initiative> result = service.findInitiativesByCategoryAndTitle(null,null);
        assertResultsForInvalidValues(result);
    }

    @Test
    public void findInitiativesByClubAndTitle() {
        when(repository.findInitiativeByClubAndTitle(anyString(), anyString())).thenReturn(Arrays.asList(getTestInititiveData()));
        List<Initiative> result = service.findInitiativesByClubAndTitle("some club","some title");
        assertResult(result);
    }

    @Test
    public void findInitiativesByClubAndTitleWhenNullIsPassedForClub() {
        List<Initiative> result = service.findInitiativesByClubAndTitle(null,"some title");
        assertResultsForInvalidValues(result);
    }

    @Test
    public void findInitiativesByClubAndTitleWhenNullIsPassedForTitle() {
        List<Initiative> result = service.findInitiativesByClubAndTitle("Some club",null);
        assertResultsForInvalidValues(result);
    }

    @Test
    public void findInitiativesByClubAndTitleWhenNullIsPassed() {
        List<Initiative> result = service.findInitiativesByClubAndTitle(null,null);
        assertResultsForInvalidValues(result);
    }

    @Test
    public void findInitiativesBySearchTerm() {
        when(repository.findInitiativeByTitleContainingIgnoreCaseOrInitleadContainingIgnoreCaseOrClubIgnoreCaseContainingOrCategoryContainingIgnoreCaseOrDescriptionContainingIgnoreCase("something", "something", "something", "something", "something"))
                .thenReturn(Arrays.asList(getTestInititiveData()));
        List<Initiative> result = service.findInitiativeBySearchTerm(SEARCH_TERM);
        Assert.assertEquals(1, result.size());
    }

    @Test
    public void findInitiativesByClubAndSearchTerm() {

        when(repository.findInitiativeByTitleContainingIgnoreCaseOrInitleadContainingIgnoreCaseOrClubIgnoreCaseContainingOrCategoryContainingIgnoreCaseOrDescriptionContainingIgnoreCase(SEARCH_TERM, SEARCH_TERM, SEARCH_TERM, SEARCH_TERM, SEARCH_TERM))
                .thenReturn(Arrays.asList(getTestInititiveData()));
        List<Initiative> result = service.findInitiativeByClubAndSearchTerm(CLUB, SEARCH_TERM);
        Assert.assertEquals(1, result.size());
    }

    @Test
    public void findInitiativesByCategoryndSearchTerm() {
        when(repository.findInitiativeByTitleContainingIgnoreCaseOrInitleadContainingIgnoreCaseOrClubIgnoreCaseContainingOrCategoryContainingIgnoreCaseOrDescriptionContainingIgnoreCase(SEARCH_TERM, SEARCH_TERM, SEARCH_TERM, SEARCH_TERM, SEARCH_TERM))
                .thenReturn(Arrays.asList(getTestInititiveData()));
        List<Initiative> result = service.findInitiativeByCategoryAndSearchTerm(CATEGORY, SEARCH_TERM);
        Assert.assertEquals(1, result.size());
    }

    @Test
    public void findAllPagewise() {
        List<Initiative> list = Arrays.asList(getTestInititiveData());
        final Page<Initiative> page = new PageImpl<>(list);
        Pageable pageable = PageRequest.of(1, 5);
        when(repository.findAll(pageable)).thenReturn(page);
        Page<Initiative> result = service.findAll(pageable);
        assertThat(result.getTotalElements(), is(1L));
    }

    @Test
    public void findAllPagewiseWhenNullIsPassed() {
        List<Initiative> list = Arrays.asList(getTestInititiveData());
        final Page<Initiative> page = new PageImpl<>(list);
        Page<Initiative> result = service.findAll(null);
        assertThat(result, is(nullValue()));
    }

    @Test
    public void findAllPagewiseWhenZeroIsPased() {
        exception.expect(java.lang.IllegalArgumentException.class);
        exception.expectMessage("Page size must not be less than one!");
        List<Initiative> list = Arrays.asList(getTestInititiveData());
        Pageable pageable = PageRequest.of(0, 0);
        service.findAll(pageable);
    }

    @Test
    public void findAll() {
        when(repository.findAll()).thenReturn(Arrays.asList(getTestInititiveData()));
        List<Initiative> result = service.findAll();
        assertResult(result);
    }

    @Test
    public void save() {
        Initiative initiative = getTestInititiveData();
        when(repository.findInitiativeByTitle(anyString())).thenReturn(null);
        when(repository.save(initiative)).thenReturn(getTestInititiveData());
        Initiative result = service.save(initiative);
        assertInitiative(result);
    }

    /* Not required
    @Test
    public void saveNullInitiative() {
        exception.expect(InitiativeException.class);
        exception.expectMessage("Invalid Initiative");
        service.save(null);
    }*/

    @Test
    public void saveDuplicateInitiative() {
        exception.expect(InitiativeException.class);
        exception.expectMessage("Initiative title already exists!");
        Initiative initiative = getTestInititiveData();
        List<Initiative> initiatives = Arrays.asList(initiative);
        when(repository.findInitiativeByTitle(anyString())).thenReturn(initiatives);
        service.save(initiative);
    }

    @Test
    public void update() {
        Initiative initiative = getTestInititiveData();
        initiative.setId(1);
        when(repository.save(initiative)).thenReturn(getTestInititiveData());
        when(repository.findInitiativeByTitle(anyString())).thenReturn(Arrays.asList(initiative));
        Initiative result = service.update(initiative);
        assertInitiative(result);
    }

    /* Not required
    @Test
    public void updateNullInitiative() {
        exception.expect(InitiativeException.class);
        exception.expectMessage("Invalid Initiative");
        service.update(null);
    }*/

    /* Not required
    @Test
    public void updateDuplicateInitiative() {
        exception.expect(InitiativeException.class);
        exception.expectMessage("Initiative title already exists!");
        Initiative initiative = getTestInititiveData();
        initiative.setId(1);
        List<Initiative> initiatives = Arrays.asList(initiative);
        when(repository.findInitiativeByTitle(anyString())).thenReturn(initiatives);

        Initiative Updating = getTestInititiveData();
        Updating.setId(2);
        service.update(Updating);
    }*/


    @Test
    public void findById() {
        when(repository.findById(anyLong())).thenReturn(Optional.of(getTestInititiveData()));
        Optional<Initiative> result = service.findById(0l);
        assertInitiative(result.get());
    }

    @Test
    public void delete() {
        Initiative initiative = getTestInititiveData();
        doNothing().when(repository).deleteById(new Long(12345));
        service.delete(new Long(12345));
        verify(repository, atLeast(1)).deleteById(new Long(12345));
    }

   /* @Test
    public void deleteNullInitiative() {
        exception.expect(InitiativeException.class);
        exception.expectMessage("Invalid Initiative");
        service.delete(null);
    }*/


    private Initiative getTestInititiveData() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return Initiative.builder()
                .title("Test title")
                .category("AND Tools")
                .club("Dekker")
                .initlead("Marty Mcfly")
                .active(true)
                .description("This is very descriptive!")
                .createddate(LocalDate.parse("11-12-2018", formatter))
                .build();
    }

    private void assertResult(List<Initiative> result) {
        assertThat(result, is(notNullValue()));
        assertThat(result.size(), equalTo(1));
        assertInitiative(result.get(0));
    }

    private void assertResultsForInvalidValues(List<Initiative> result) {
        assertThat(result, is(notNullValue()));
        assertThat(result.size(), is(0));
    }

    private void assertInitiative(Initiative initiative) {
        assertThat(initiative, is(notNullValue()));
        assertThat(initiative.getTitle(), is(equalTo("Test title")));
        assertThat(initiative.getCategory(), is(equalTo("AND Tools")));
        assertThat(initiative.getClub(), is(equalTo("Dekker")));
        assertThat(initiative.getInitlead(), is(equalTo("Marty Mcfly")));
        assertThat(initiative.getActive(), is(true));
        assertThat(initiative.getCreateddate(), is(lessThan(LocalDate.now())));
        assertThat(initiative.getLinks(), is(nullValue()));
    }

}
