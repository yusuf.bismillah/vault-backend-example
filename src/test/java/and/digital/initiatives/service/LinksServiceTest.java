package and.digital.initiatives.service;

import and.digital.initiatives.exception.LinkException;
import and.digital.initiatives.model.Link;
import and.digital.initiatives.repository.LinkRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.*;

public class LinksServiceTest {
    private LinksService service;

    LinkRepository repository = mock(LinkRepository.class);

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Before
    public void setup() {
        service = spy(new LinksService(repository));
    }

    @Test
    public void findAllLinks() {
        when(repository.findAll()).thenReturn(Arrays.asList(getTestLinkData()));
        List<Link> result = service.findAll();
        assertResult(result);
    }

    @Test
    public void findLinksByInvalidId() {
        Link result = service.findById(1);
        assertThat(result, is(nullValue()));
    }

    @Test
    public void findLinksByValidId() {
        when(repository.findById(anyLong())).thenReturn(getTestLinkData());
        Link result = service.findById(1);
        assertLink(result);
    }

    @Test
    public void testSaveValidLink() {
        Link link = getTestLinkData();
        when(repository.save(any())).thenReturn(link);
        Link result = service.save(link);
        assertLink(result);
    }

    @Test
    public void testSaveInvalidLink() {
        exception.expect(LinkException.class);
        exception.expectMessage("Invalid Link");
        service.save(null);
    }

    @Test
    public void testDeleteInvalidLink() {
        exception.expect(LinkException.class);
        exception.expectMessage("Invalid Link");
        service.delete(null);
    }

    @Test
    public void testDeleteValidLink() {
        Link link = getTestLinkData();
        doNothing().when(repository).delete(link);
        service.delete(link);
        verify(repository, atLeast(1)).delete(link);
    }

    private Link getTestLinkData() {
        return Link.builder()
                .link("test link")
                .type("test type")
                .id(1l)
                .initiative(null)
                .build();
    }

    private void assertResult(List<Link> result) {
        assertThat(result, is(notNullValue()));
        assertThat(result.size(), equalTo(1));
        assertLink(result.get(0));
    }

    private void assertLink(Link link) {
        assertThat(link, is(notNullValue()));
        assertThat(link.getLink(), is(equalTo("test link")));
        assertThat(link.getType(), is(equalTo("test type")));
        assertThat(link.getId(), is(equalTo(1l)));
    }
}
