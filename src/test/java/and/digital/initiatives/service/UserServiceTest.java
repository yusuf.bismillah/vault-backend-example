package and.digital.initiatives.service;

import and.digital.initiatives.exception.UserException;
import and.digital.initiatives.model.User;
import and.digital.initiatives.repository.UserRepository;
import org.junit.Rule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class UserServiceTest {
    private UserService userService;
    UserRepository userRepository;
    private static final String FIRST_NAME = "Test";
    private static final String LAST_NAME = "User";
    private static final String CLUB = "Newton";
    private static final String SQUAD = "Prometheus";
    private static final String EMAIL = "test@and.digital";

    @Rule
    public final ExpectedException exception = ExpectedException.none();
    @BeforeEach
    public void setUp() {
        userRepository = mock(UserRepository.class);
        userService  = spy(new UserService(userRepository));
    }

    @Test
    void createUser() {
        User userToCreate = buildUserWitId(1l);
        when(userRepository.save(buildUserWithoutId())).thenReturn(userToCreate);
        when(userRepository.findUsersByEmail("test@and.digital")).thenReturn(new ArrayList<>());
        User user = userService.createUser(buildUserWithoutId());
        verify(userService, times(1)).createUser(buildUserWithoutId());
        assertEquals(user, userToCreate);
    }

    @Test
    void createUserWithException() {
        User userToCreate = buildUserWitId(1l);
        when(userRepository.save(buildUserWithoutId())).thenReturn(userToCreate);
        when(userRepository.findUsersByEmail("test@and.digital")).thenReturn(Arrays.asList(userToCreate));
        Exception exception = assertThrows(UserException.class, () -> {
            userService.createUser(buildUserWithoutId());
        });
        assertEquals(exception.getMessage(), "User with Email id" + userToCreate.getEmail() + " already exist");
    }

    @Test
    void updateUser() {
        User userToUpdate = buildUserWitId(1l);
        when(userRepository.save(userToUpdate)).thenReturn(userToUpdate);
        when(userRepository.findUsersByEmail("test@and.digital")).thenReturn(new ArrayList<>());
        User user = userService.updateUser(userToUpdate);
        verify(userService, times(1)).updateUser(userToUpdate);
        assertEquals(user, userToUpdate);
    }

    @Test
    void getUserById() {
        when(userRepository.findById(1l)).thenReturn(Optional.of(buildUserWitId(1l)));
        when(userRepository.findUsersByEmail("test@and.digital")).thenReturn(new ArrayList<>());
        Optional<User> user = userService.getUserById(1l);
        verify(userService, times(1)).getUserById(1l);
        assertEquals(user.get(), buildUserWitId(1l));
    }

    @Test
    void deleteUser() {
        doNothing().when(userRepository).deleteById(1l);
        userService.deleteUser(1l);
        verify(userService, times(1)).deleteUser(1l);
    }

    @Test
    void findUserByFirstNameOrLastName() {
        List<User> result = Arrays.asList(buildUserWitId(1l));
        when(userRepository.findUserByFirstNameLastName(FIRST_NAME, LAST_NAME)).thenReturn(result);
        List<User> users = userService.findUserByFirstNameOrLastName(FIRST_NAME, LAST_NAME);
        verify(userService, times(1)).findUserByFirstNameOrLastName(FIRST_NAME, LAST_NAME);
        assertEquals(users.size(), result.size());
    }

    private User buildUserWithoutId() {
        return User.builder()
                .firstName(FIRST_NAME)
                .lastName(LAST_NAME)
                .email(EMAIL)
                .club(CLUB)
                .squad(SQUAD)
                .build();
    }

    private User buildUserWitId(Long id) {
        return User.builder()
                .id(id)
                .firstName(FIRST_NAME)
                .lastName(LAST_NAME)
                .email(EMAIL)
                .club(CLUB)
                .squad(SQUAD)
                .build();
    }
}