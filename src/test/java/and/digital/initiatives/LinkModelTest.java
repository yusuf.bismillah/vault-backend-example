package and.digital.initiatives;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;


import and.digital.initiatives.model.Initiative;
import and.digital.initiatives.model.Link;
import and.digital.initiatives.repository.LinkRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
@Ignore
public class LinkModelTest {
 
    @Autowired
    private TestEntityManager entityManager;
 
    @Autowired
    private LinkRepository linkRepository;
    
    
    private Initiative testInitiative;
    
    private Link testLink;
    
    @Before
    public void setUp() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    	
    	testInitiative = Initiative.builder()
                .title("Test Initiative Title 123")
                .category("AND Tools")
                .club("Dekker")
                .initlead("Marty Mcfly")
                .active(true)
                .description("This is very descriptive!")
                .createddate(LocalDate.parse("11-12-2018", formatter))
                .build();
    	
    	testLink = Link.builder()
    			.type("Google Drive")
    			.link("https://www.google.com/drive/")
    			.initiative(testInitiative)
    			.build();
    	
    	entityManager.persist(testInitiative);
    	entityManager.persist(testLink);
        entityManager.flush();
    }
    
    @Test
    public void testLinkInfo() {
    	
    	String type = testLink.getType();
    	String link = testLink.getLink();
    	Initiative parentInitiative = testLink.getInitiative();
    	
    	Assert.assertTrue(type == "Google Drive");
    	Assert.assertTrue(link == "https://www.google.com/drive/");
    	Assert.assertTrue(parentInitiative.equals(testInitiative));
    }

    @After
    public void tearDown() {
    	entityManager.remove(testInitiative);
    	entityManager.remove(testLink);
        entityManager.flush();
    }
}