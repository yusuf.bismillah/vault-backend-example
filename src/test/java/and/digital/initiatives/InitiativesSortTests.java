package and.digital.initiatives;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;

import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import and.digital.initiatives.model.Initiative;
import and.digital.initiatives.repository.InitiativeRepository;
import and.digital.initiatives.sorter.InitiativesSortByDate;
import and.digital.initiatives.sorter.InitiativesSortByRelevance;
import and.digital.initiatives.sorter.InitiativesSortByTitle;

//TODO check and remove later
@RunWith(SpringRunner.class)
@DataJpaTest
@Ignore
public class InitiativesSortTests {
 
    @Autowired
    private TestEntityManager entityManager;
 
    @Autowired
    private InitiativeRepository initiativeRepository;
    
    private Initiative testInitiative1;
    private Initiative testInitiative2;
    private Initiative testInitiative3;
 
    @Before
    public void setUp() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    	
    	testInitiative1 = Initiative.builder()
                .title("B Test docker Initiative Title 123")
                .category("AND Tools")
                .club("Dekker")
                .initlead("Marty Mcfly")
                .active(true)
                .description("This is very descriptive!")
                .createddate(LocalDate.parse("11-12-2019", formatter))
                .build();
    	
    	testInitiative2 = Initiative.builder()
                .title("A Test test Initiative Title 123")
                .category("AND Tools")
                .club("Dekker")
                .initlead("Marty Mcfly")
                .active(true)
                .description("This is very descriptive!")
                .createddate(LocalDate.parse("11-10-2018", formatter))
                .build();
    	
    	testInitiative3 = Initiative.builder()
                .title("C Test sample Initiative Title 123")
                .category("AND Tools")
                .club("Dekker")
                .initlead("Marty Mcfly")
                .active(true)
                .description("This is very descriptive!")
                .createddate(LocalDate.parse("11-12-2018", formatter))
                .build();
    	
    	entityManager.persist(testInitiative1);
    	entityManager.persist(testInitiative2);
    	entityManager.persist(testInitiative3);
        entityManager.flush();
    }
    
    
    @Test
    public void testSortByTitle() {
    	List<Initiative> list = initiativeRepository.findAll();
    	list.sort(new InitiativesSortByTitle());
    	Assert.assertEquals(list.get(0), testInitiative2);
    	Assert.assertEquals(list.get(1), testInitiative1);
    	Assert.assertEquals(list.get(2), testInitiative3);
    }
    
    @Test
    public void testSortByDate() {
    	List<Initiative> list = initiativeRepository.findAll();
    	list.sort(new InitiativesSortByDate());
    	Collections.reverse(list);
    	Assert.assertEquals(list.get(0), testInitiative1);
    	Assert.assertEquals(list.get(1), testInitiative3);
    	Assert.assertEquals(list.get(2), testInitiative2);
    }
    
    @Test
    public void testSortByRelevance() {
    	List<Initiative> list = initiativeRepository.findAll();
    	list.sort(new InitiativesSortByRelevance());
    	Assert.assertEquals(list.get(0), testInitiative1);
    	Assert.assertEquals(list.get(1), testInitiative2);
    	Assert.assertEquals(list.get(2), testInitiative3);
    }
    
    @After
    public void tearDown() {
    	entityManager.remove(testInitiative1);
    	entityManager.remove(testInitiative2);
    	entityManager.remove(testInitiative3);
        entityManager.flush();
    }
}